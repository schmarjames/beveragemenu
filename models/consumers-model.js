var db = require('./db').db;
var Sequelize = require('./db').ModelObj;
var Op = Sequelize.Op;

const Consumer = db.define('consumer', {
  caffineTotal: {
    type: Sequelize.INTEGER,
    field: 'caffine_total'
  },
  caffineMax: {
    type: Sequelize.INTEGER,
    field: 'caffine_max'
  }
});

Consumer.getAUser = (id) => {
  return Consumer.find({
    where: {
      id: id
    }
  })
};

Consumer.updateCaffineTotal = (userId, totalCaffine) => {
  return Consumer.update({
    caffineTotal: totalCaffine,
  }, {
    where: {
      id: {
        [Op.eq]: userId
      }
    }
  }).then(() => {
    return Consumer.getAUser(userId)
  })
}

// force: true will drop the table if it already exists
/*Consumer.sync({force: true}).then(() => {
  // Table created
  return Consumer.create(
    {
      caffineTotal: 0,
      caffineMax: 500
    }
  );
});*/

module.exports = Consumer;
