var db = require('./db').db;
var Sequelize = require('./db').ModelObj;

const Drink = db.define('drink', {
  name: {
    type: Sequelize.STRING
  },
  description: {
    type: Sequelize.STRING
  },
  caffineAmount: {
    type: Sequelize.INTEGER,
    field: 'caffine_amount'
  }
});

Drink.getADrink = (id) => {
  return Drink.find({
    where: {
      id: id
    }
  })
};

// force: true will drop the table if it already exists
/*Drink.sync({force: true}).then(() => {
  // Table created
  return Drink.bulkCreate(
    [
      {
        name: 'Monster Ultra Sunrise',
        description: 'A refreshing orange beverage that has 75mg of caffeine per serving. Every can has two servings.',
        caffineAmount: 150
      },
      {
        name: 'Black Coffee',
        description: 'The classic, the average 8oz. serving of black coffee has 95mg of caffeine.',
        caffineAmount: 95
      },
      {
        name: 'Americano',
        description: 'Sometimes you need to water it down a bit... and in comes the americano with an average of 77mg. of caffeine per serving.',
        caffineAmount: 77
      },
      {
        name: 'Sugar free NOS',
        description: 'Another orange delight without the sugar. It has 130 mg. per serving and each can has two servings.',
        caffineAmount: 260
      },
      {
        name: '5 Hour Energy',
        description: 'And amazing shot of get up and go! Each 2 fl. oz. container has 200mg of caffeine to get you going.',
        caffineAmount: 200
      }
    ]
  );
});*/

module.exports = Drink;
