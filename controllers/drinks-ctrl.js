var Drink = require('../models/drinks-model');

const getDrinks = (req, res, next) => {
  Drink.findAll().then(drinks => {
    res.json({ data: drinks });
  });
}

module.exports = { getDrinks };
