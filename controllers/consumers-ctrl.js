var Consumer = require('../models/consumers-model');
var Drink = require('../models/drinks-model');
var Op = require('../models/db').ModelObj.Op;

const getCurrentStatus = (req, res, next) => {
  var id = req.params.id;

  if (!id) {
      res.json({ error: "user not found." });
  }

  Consumer.getAUser(id).then(consumer => {
    res.json({ data: consumer });
  });
}

const getConsumers = (req, res, next) => {
  Consumer.findAll().then(consumers => {
    res.json({ data: consumers });
  });
}

const updateCaffineAmount = (req, res, next) => {
  var id = req.params.id;
  var drinkId = req.body.drinkId;
  var amount = req.body.amount;

  if (!id || isNaN(drinkId) || isNaN(amount)) {
      res.json({ error: "data is missing." });
  } else {
    // update consumers caffine intake
    let userData;
    let drinkData;

    Consumer.getAUser(id)
      .then((user) => {
        userData = user;
        return Drink.getADrink(drinkId)
      })
      .then((drink) => {
        drinkData = drink;
        var caffineTotalOrder = beverageOrderTotalCaffine(drinkData.caffineAmount, amount);

        if (reachedCaffineMax(userData, caffineTotalOrder)) {
          throw new Error(res.json({ error: 'Reached Caffine Max!'}));
        } else {
          var newTotal = caffineTotalOrder+userData.caffineTotal;
          return Consumer.updateCaffineTotal(userData.id, newTotal);
        }
      })
      .then((consumer) => {
        res.json({ data: consumer });
      });
  }
}

const resetCaffineAmount = (req, res, next) => {
  var id = req.params.id;

  if (!id) {
      res.json({ error: "user not found." });
  }

  Consumer.updateCaffineTotal(id, 0)
    .then((consumer) => {
      res.json({ data: consumer });
    });

}

const beverageOrderTotalCaffine = (caffineAmount, beverageAmount) => {
  return caffineAmount*beverageAmount;
}

const reachedCaffineMax = (userData, orderCaffineTotal) => {
  var caffineMax = userData.caffineMax;
  var currentCaffineTotal = userData.caffineTotal;
  var threshold = caffineMax - currentCaffineTotal;
  return orderCaffineTotal > threshold;
}

module.exports = {
  getConsumers,
  getCurrentStatus,
  updateCaffineAmount,
  resetCaffineAmount
};
