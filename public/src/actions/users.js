import axios from 'axios';

export const ALERT = 'ALERT';

export const GET_USERS = 'GET_USERS';
export const CHOSE_USER = 'CHOSE_USER';
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS';
export const GET_USERS_FAILURE = 'GET_USERS_FAILURE';

export const SELECTED_DRINK = 'SELECTED_DRINK';
export const SELECTED_DRINK_AMOUNT = 'SELECTED_DRINK_AMOUNT';

export const PLACE_ORDER = 'PLACE_ORDER';
export const PLACE_ORDER_SUCCESS = 'PLACE_ORDER_SUCCESS';
export const PLACE_ORDER_FAILURE = 'PLACE_ORDER_FAILURE';

export const RESET_SELECTION = 'RESET_SELECTION';
export const RESET_USER = 'RESET_USER';
export const RESET_SUCCESS = 'RESET_SUCCESS';
export const RESET_FAILURE = 'RESET_FAILURE';

const ROOT_URL = location.href.indexOf('localhost') > 0 ? 'http://localhost:3000/api' : '/api';

const displayAlert = (message) => {
  return {
    type: ALERT,
    message
  }
}

const getUsers = () => {
  const request = axios({
    method: 'get',
    url: `${ROOT_URL}/consumers`
  });

  return {
    type: GET_USERS,
    payload: request
  }
}

const getUsersSuccess = (data) => {
  console.log('user succ', data.data.data);
  return {
    type: GET_USERS_SUCCESS,
    payload: data.data.data
  };
}

const getUsersFailure = (error) => {
  return {
    type: GET_USERS_FAILURE,
    payload: error
  };
}

const placeOrder = (data) => {
  const request = axios({
    method: 'post',
    data: {
      drinkId: data.selectedDrink,
      amount: data.selectedDrinkAmount
    },
    url: `${ROOT_URL}/consumers/${data.selectedUserId}/drink`
  });

  return {
    type: PLACE_ORDER,
    payload: request
  }
}

const resetUser = (id) => {
  const request = axios({
    method: 'post',
    url: `${ROOT_URL}/consumers/${id}/reset`
  });

  return {
    type: RESET_USER,
    payload: request
  }
}

const resetSuccess = () => {
  return {
    type: RESET_SUCCESS
  };
}

const resetFailure = (error) => {
  return {
    type: RESET_FAILURE,
    payload: error
  };
}

const placeOrderSuccess = (data) => {
  console.log('order succ', data);
  return {
    type: PLACE_ORDER_SUCCESS,
    payload: data.data.data
  };
}

const placeOrderFailure = (error) => {
  return {
    type: PLACE_ORDER_FAILURE,
    payload: error
  };
}

const choseUser = (user) => {
  return {
    type: CHOSE_USER,
    user
  };
}

const selectDrink = (drink) => {
  return {
    type: SELECTED_DRINK,
    drink
  };
}

const selectDrinkAmount = (drinkAmount) => {
  return {
    type: SELECTED_DRINK_AMOUNT,
    drinkAmount
  };
}

const resetSelection = (drink) => {
  return {
    type: RESET_SELECTION
  };
}

export {
  displayAlert,
  choseUser,
  getUsersFailure,
  getUsersSuccess,
  getUsers,
  placeOrderFailure,
  placeOrderSuccess,
  placeOrder,
  resetFailure,
  resetSelection,
  resetSuccess,
  resetUser,
  selectDrinkAmount,
  selectDrink
}
