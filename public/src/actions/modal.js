export const OPEN = 'OPEN';
export const CLOSE = 'CLOSE';

const open = (data) => {
  return {
    type: OPEN,
    drink: data
  };
}

const close = () => {
  return {
    type: CLOSE
  };
}

export { open, close }
