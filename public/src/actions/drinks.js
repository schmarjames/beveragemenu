import axios from 'axios';
export const GET_BEVERAGE_MENU = 'GET_BEVERAGE_MENU';
export const GET_BEVERAGE_MENU_SUCCESS = 'GET_BEVERAGE_MENU_SUCCESS';
export const GET_BEVERAGE_MENU_FAILURE = 'GET_BEVERAGE_MENU_FAILURE';

const ROOT_URL = location.href.indexOf('localhost') > 0 ? 'http://localhost:3000/api' : '/api';

const getBeverageMenu = () => {

  const request = axios({
    method: 'get',
    url: `${ROOT_URL}/drinks`
  });

  return {
    type: GET_BEVERAGE_MENU,
    payload: request
  }
}

const getBeverageMenuSuccess = (data) => {
  return {
    type: GET_BEVERAGE_MENU_SUCCESS,
    payload: data.data.data
  };
}

const getBeverageMenuFailure = (error) => {
  return {
    type: GET_BEVERAGE_MENU_FAILURE,
    payload: error
  };
}

export { getBeverageMenu, getBeverageMenuSuccess, getBeverageMenuFailure }
