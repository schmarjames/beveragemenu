import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux'
import configureStore from './store/configureStore.js';
import Root from './containers/Root'

const store = configureStore();

render(
  <Root store={store} />,
  document.getElementById('body')
)
