import React , { Component } from 'react';
import '../../style/app.scss';

import ModalContainer from '../containers/ModalContainer';

class App extends Component {
	componentWillMount() {
    this.props.loadDrinks();
		this.props.loadUsers();
		this.aboveThreshold = false;
  }

	componentWillReceiveProps(nextProps) {
		if (nextProps.user && nextProps.user.users.length == 1) {
			this.props.choseUser(nextProps.user.users.pop());
		}
	}

	getTheCurrentAmount(e) {
		const amount = e.data || e.target.value;

		if (parseInt(amount) === this.props.selectedDrinkAmount) {
			return;
		}

		if (e.data || e.target.value) {
			const threshold = this.props.caffineMax - this.props.caffineTotal;
			const drinkMenu = this.props.drinks.drinks;
			const chosenDrink = drinkMenu.filter(drink => drink.id === this.props.selectedDrink).pop();
			const totalCaffine = chosenDrink.caffineAmount * parseInt(amount);

			if (totalCaffine <= threshold) {
				this.aboveThreshold = false;
				this.props.submitAmount(parseInt(amount));
				this.props.emitAlert(null);

			} else {
				this.aboveThreshold = true;
				this.props.submitAmount(null);
				this.prepAlert("Chosen amount exceeds limit!");
			}
		}

	}

	displayMessage() {
		const message = this.props.alert || this.props.error;
		if (message) {
			return (
				<p>{message}</p>
			)
		}
	}

	displayBeverages(drinks) {
		const threshold = this.props.caffineMax - this.props.caffineTotal;
		const drinksList = drinks.map((drink, index) => {
			const notAvail = drink.caffineAmount >= threshold;
				return (
				<li className="entry" style={{opacity: (notAvail) ? 0.2 : 1}}>
					<div className="title">
						<input
							onChange={() => { this.props.submitDrink(drink) }}
							name="select-drink"
							type="radio"
							disabled={notAvail}
							checked={drink.id === this.props.selectedDrink}
							value={drink.id}
							/>
						<label>{drink.name}</label>
					</div>
					<div className="col-sm-2 desc-wrapper">
						<a href="#" className="desc-link" onClick={(e) => { this.showDescription(e, drink) }}>
							<span className="glyphicon glyphicon-question-sign"></span>
						</a>
					</div>
				</li>
				)
		})
		return (drinksList)
	}

	displayAmountField() {
		if (this.props.user && this.props.selectedDrink) {
			return (
				<input
					name="amount"
					type="number"
					min="1"
					placeholder="How many?"
					onChange={this.getTheCurrentAmount.bind(this)}
					onMouseUp={this.getTheCurrentAmount.bind(this)}
					value={this.props.selectedDrinkAmount}
					/>
			)
		}
	}

	displayCaffineConsumptionTotal() {
		return `${this.props.caffineTotal}mg`
	}

	displayUserMax() {
		if (this.props.user) {
			return (
				<span>{`MAX: ${this.props.caffineMax}`}</span>
			)
		}
	}

	isDisabled() {
		return (!this.props.user || !this.props.selectedDrink || !this.props.selectedDrinkAmount || this.aboveThreshold);
	}

	isResetDisabled() {
		return this.props.caffineTotal === 0;
	}

	prepAlert(message) {
		if (!this.props.alert) {
			this.props.emitAlert(message);
		}
	}

	resetUser(e) {
		e.preventDefault();
		this.props.resetUserIntake(this.props.selectedUserId);
	}

	showDescription(e, drink) {
		e.preventDefault();
		this.props.openModal(drink);
	}

	serveDrink(e) {
    e.preventDefault();

		if (this.props.alert) {
			this.props.emitAlert(null);
		}

		this.props.submitOrder({
			selectedUserId: this.props.selectedUserId,
			selectedDrink: this.props.selectedDrink,
			selectedDrinkAmount: this.props.selectedDrinkAmount
		});
	}

  render() {
		const {asyncValidating, handleSubmit, submitting} = this.props;

		const drinks = this.props.drinks.drinks;
		const users = this.props.user;
		const caffineTotal = this.props.caffineTotal;

    return (
      <div className="container-fluid wrapper">
				<ModalContainer />
				<h1 className="menu-title">Caffeine Beverage Menu</h1>
				<form className="row">
						 <ul className="col-sm-6 list-wrapper">
							{ this.displayBeverages(drinks) }
						 </ul>
						 <div className="col-sm-6">
						 	 <div className="status-bar col-sm-12">
							 	<div className="alert-status col-sm-10">
									{this.displayMessage()}
								</div>
								<div className="user-max-status col-sm-2">
									{this.displayUserMax()}
								</div>
							 </div>
							 <div className="col-sm-12">
								 <span className="total">
									 {this.displayCaffineConsumptionTotal()}
								 </span>
							 </div>
							 <div className="col-sm-12 field-slot">
								 {this.displayAmountField()}
							 </div>
							 <div className="col-sm-12 field-slot">
								 <input
									 onClick={(e) => { this.serveDrink(e) }}
									 disabled={this.isDisabled()}
									 name="submit"
									 type="submit"
									 value="Submit Order"
									 />
							 </div>
							 <div className="col-sm-12 field-slot">
								 <button
									 onClick={(e) => { this.resetUser(e) }}
									 disabled={this.isResetDisabled()}
									 name="reset"
									 >
									 Reset Consumption
								 </button>
							 </div>
						 </div>
				</form>
      </div>
    );
  }
}

export default App;
