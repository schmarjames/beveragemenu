import React , { Component } from 'react';
import '../../style/app.scss';

class Modal extends Component {

  close(e) {
    e.preventDefault();
    this.props.close();
  }

  displayDrinkDetails() {
    if (this.props.drink) {
      return (
        <div className="desc-section container-fluid">
          <h2>{this.props.drink.name}</h2>
          <p>{this.props.drink.description}</p>
        </div>
      )
    }
  }

  render() {
    const display = (this.props.open) ? 'block' : 'none';

    return (
      <div className="modal-wrapper col-sm-12" style={{display}}>
        <a href="#" className="close-link" onClick={(e) => { this.close(e) }}>
          <span className="glyphicon glyphicon-remove"></span>
        </a>
        {this.displayDrinkDetails()}
      </div>
    )
  }
}

export default Modal;
