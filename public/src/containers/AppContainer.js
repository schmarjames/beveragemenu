import React, { Component } from 'react';
import { connect } from 'react-redux';
import { displayAlert, getUsers, getUsersSuccess, getUsersFailure, choseUser, resetUser, resetFailure, resetSuccess, selectDrink, selectDrinkAmount, placeOrder, placeOrderSuccess, placeOrderFailure } from '../actions/users';
import { getBeverageMenu, getBeverageMenuSuccess, getBeverageMenuFailure } from '../actions/drinks';
import { open } from '../actions/modal';

import App from '../components/App.js';

const mapDispatchToProps = (dispatch) => {
  return {
      choseUser: (user) => {
        dispatch(choseUser(user));
      },

     emitAlert: (message) => {
       dispatch(displayAlert(message));

       if (message) {
         setTimeout(() => {
           dispatch(displayAlert(null));
         }, 2000);
       }
     },

     loadDrinks: () => {
       dispatch(getBeverageMenu())
        .then((res) => {
          if (!res.payload.error) {
            dispatch(getBeverageMenuSuccess(res.payload));
          } else {
            dispatch(getBeverageMenuFailure(res.payload));
          }
        });
     },

     loadUsers: () => {
       dispatch(getUsers())
       .then((res) => {
         if (!res.payload.error) {
           dispatch(getUsersSuccess(res.payload));
         } else {
           dispatch(getUsersFailure(res.payload));
         }
       });
     },

     openModal: (data) => {
       dispatch(open(data))
     },

     resetUserIntake: (id) => {
       dispatch(resetUser(id))
         .then((res) => {
           if (!res.payload.error) {
             dispatch(resetSuccess(res.payload));
           } else {
             dispatch(resetFailure(res.payload));
           }
         });
     },

     submitDrink: (drink) => {
       dispatch(selectDrink(drink));
     },

     submitAmount: (amount) => {
       dispatch(selectDrinkAmount(amount));
     },

     submitOrder: (order) => {
       dispatch(placeOrder(order))
        .then((res) => {
          if (!res.payload.error) {
            dispatch(placeOrderSuccess(res.payload));
          } else {
            dispatch(placeOrderFailure(res.payload));
          }
        })
     }
  }
}

const mapStateToProps = (state) => {
  return {
    alert: state.user.alert || undefined,
    error: state.user.error || undefined,
    user: state.user,
    usersBatch: (state.user) ? state.user.user : undefined,
    drinks: state.drinks,
    loadingUsers: (state.user) ? state.user.loading : false,
    selectedUserId: (state.user) ? state.user.selectedUserId : undefined,
    selectedDrink: state.user.selectedDrink,
    selectedDrinkAmount: (state.user) ? state.user.selectedDrinkAmount : undefined,
    caffineMax: (state.user) ? state.user.caffineMax : undefined,
    caffineTotal: (state.user) ? state.user.caffineTotal : undefined,
    beverageMenu: state.drinks.drinks,
    loadingBeverages: state.drinks.loading
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(App);
