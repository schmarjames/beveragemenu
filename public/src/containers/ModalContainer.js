import React, { Component } from 'react';
import { connect } from 'react-redux';
import { close } from '../actions/modal';
import Modal from '../components/Modal.js';

const mapDispatchToProps = (dispatch) => {
  return {
     close: () => {
       dispatch(close());
     }
  }
}

const mapStateToProps = (state) => {
  return {
    open: state.modal.open,
    drink: state.modal.drink
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Modal);
