import {
  OPEN, CLOSE
} from '../actions/modal';

const INITIAL_STATE = {
  drink: null,
  open: false,
}

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case OPEN:
      return { ...state, open: true, drink: action.drink };
    case CLOSE:
      return { ...state, open: false, drink: null };
    default:
      return state;
  }
}
