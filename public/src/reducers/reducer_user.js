
import {
  ALERT, GET_USERS, GET_USERS_SUCCESS, GET_USERS_FAILURE,
  CHOSE_USER, PLACE_ORDER, PLACE_ORDER_SUCCESS, PLACE_ORDER_FAILURE,
  SELECTED_DRINK, SELECTED_DRINK_AMOUNT, RESET_SELECTION,
  RESET_USER, RESET_SUCCESS, RESET_FAILURE
} from '../actions/users';


const INITIAL_STATE = {
  alert: null,
  users: [],
  selectedUserId: null,
  caffineMax: null,
  caffineTotal: 0,
  status:null,
  error:null,
  loading: false,

  selectedDrink: null,
  selectedDrinkAmount: null,
};

export default (state = INITIAL_STATE, action) => {
  let error;
  switch(action.type) {
    case ALERT:
      return { ...state, alert: action.message };
    case GET_USERS:
      return { ...state, selectedUserId: null, users: [], caffineMax: null,  error: null, caffineTotal: 0, selectedDrink: null, selectedDrinkAmount: null, loading: true};
    case CHOSE_USER:
      return { ...state, selectedUserId: action.user.id, caffineMax: action.user.caffineMax, caffineTotal: action.user.caffineTotal,  error: null, loading: false };
    case GET_USERS_SUCCESS:
      return { ...state, users: action.payload, error: null, loading: false};
    case GET_USERS_FAILURE:
      error = action.payload || {message: action.payload.message};
      return { ...state, users: [], error: error, action, loading: false};
    case PLACE_ORDER:
      return { ...state, loading: true}
    case PLACE_ORDER_SUCCESS:
      return { ...state, caffineTotal: action.payload.caffineTotal, selectedDrink: null, selectedDrinkAmount: null,  error: null, loading: false};
    case PLACE_ORDER_FAILURE:
      error = action.payload || {message: action.payload.message};
      return { ...state, selectedDrink: null, selectedDrinkAmount: null, error: error, action, loading: false};
    case SELECTED_DRINK:
      return { ...state, error: null, selectedDrink: action.drink.id, selectedDrinkAmount: null, loading: false};
    case SELECTED_DRINK_AMOUNT:
      return { ...state, error: null, selectedDrinkAmount: action.drinkAmount, loading: false};
    case RESET_SELECTION:
      return { ...state, error: null, selectedDrink: null, selectedDrinkAmount: null, loading: false};
    case RESET_USER:
      return { ...state, selectedDrink: null, selectedDrinkAmount: null, loading: true };
    case RESET_SUCCESS:
      return { ...state, selectedDrink: null, selectedDrinkAmount: null, caffineTotal: 0, loading: false};
    case RESET_FAILURE:
      error = action.payload || {message: action.payload.message};
      return { ...state, error: error, action, loading: false};
    default:
      return state;
  }
}
