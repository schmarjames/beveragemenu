import { combineReducers } from 'redux';
import DrinksReducer from './reducer_drinks';
import ModalReducer from './reducer_modal';
import UserReducer from './reducer_user';

import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  drinks: DrinksReducer,
  modal: ModalReducer,
  user: UserReducer,
  form: formReducer, // <-- redux-form
});

export default rootReducer;
