import {
  GET_BEVERAGE_MENU, GET_BEVERAGE_MENU_SUCCESS, GET_BEVERAGE_MENU_FAILURE
} from '../actions/drinks';

const INITIAL_STATE = {
  drinks: [],
  error: null,
  loading: false
}

export default (state = INITIAL_STATE, action) => {
  let error;
  switch(action.type) {
    case GET_BEVERAGE_MENU:
      return { ...state, drinks: [], error: null, loading: true};
    case GET_BEVERAGE_MENU_SUCCESS:
      return { ...state, drinks: action.payload, error:null, loading: false};
    case GET_BEVERAGE_MENU_FAILURE:
      error = action.payload || {message: action.payload.message};
      return { ...state, drinks: [], error: error, loading: false};
    default:
      return state;
  }
}
