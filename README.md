# Beverage Menu App

- Select one of the beverages.
- Input the amount for the chosen beverage.
- After submitting your order, the caffeine total will update.
- The drinks on the menu will become disabled if there caffeine amount exceeds your limit.
- You can reset your caffeine consumption by selecting the reset button.

#API
- api/consumers
- api/consumers/:id
- api/consumers/:id/drink
- api/consumers/:id/reset

- api/drinks
