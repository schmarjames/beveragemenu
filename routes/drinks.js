var express = require('express');
var router = express.Router();
var sequelize = require('sequelize');
var drinksCtrl = require('../controllers/drinks-ctrl');

router.get('/drinks', drinksCtrl.getDrinks);

module.exports = router;
