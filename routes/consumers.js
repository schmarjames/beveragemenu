var express = require('express');
var router = express.Router();
var sequelize = require('sequelize');
var ConsumerCtrl = require('../controllers/consumers-ctrl');

router.get('/consumers', ConsumerCtrl.getConsumers);
router.get('/consumers/:id', ConsumerCtrl.getCurrentStatus);
router.post('/consumers/:id/drink', ConsumerCtrl.updateCaffineAmount);
router.post('/consumers/:id/reset', ConsumerCtrl.resetCaffineAmount);


module.exports = router;
